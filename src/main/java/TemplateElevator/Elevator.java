package TemplateElevator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-08-21.
 */
public class Elevator {

    protected List<Floor> mAvailableFloors;
    protected List<Floor> mRequesFloors = new ArrayList<>();
    protected Floor mCurrentFloor;
    protected boolean mServiceBreak = false;
    protected Weight mMaxLoad;
    Direction mDirection = Direction.DOWN;
    QueueChecker queueChecker;



    public List<Floor> getmAvailableFloors() {
        return mAvailableFloors;
    }

    public QueueChecker getQueueChecker() {
        return queueChecker;
    }

    public void setQueueChecker(QueueChecker queueChecker) {
        this.queueChecker = queueChecker;
    }

    public List<Floor> getAvailableFloors(){
        return mAvailableFloors;
    }

    public void setmAvailableFloors(List<Floor> mAvailableFloors){
        this.mAvailableFloors = mAvailableFloors;
    }

    public List<Floor> getmRequesFloors() {
        return mRequesFloors;
    }

    public void setmRequesFloors(List<Floor> mRequesFloors) {
        this.mRequesFloors = mRequesFloors;
    }

    public Floor getmCurrentFloor() {
        return mCurrentFloor;
    }

    public void setmCurrentFloor(Floor mCurrentFloor) {
        this.mCurrentFloor = mCurrentFloor;
    }

    public boolean ismServiceBreak() {
        return mServiceBreak;
    }

    public void setmServiceBreak(boolean mServiceBreak) {
        this.mServiceBreak = mServiceBreak;
    }

    public Weight getmMaxLoad() {
        return mMaxLoad;
    }

    public void setmMaxLoad(Weight mMaxLoadKg) {
        this.mMaxLoad = mMaxLoadKg;

    }

    public Direction getmDirection() {
        return mDirection;
    }

    public void setmDirection(Direction mDirection) {
        this.mDirection = mDirection;
    }


    @Override
    public String toString() {

        return super.toString() +
                " Dostepne piętra: " + mAvailableFloors + "\n Obecne piętro: " + mCurrentFloor + "\n Maksymalna ładowność: "
                + mMaxLoad + "\n Kierunek, w ktorym jade: " + mDirection;
    }

    public void nextStep(){


    }
}
//    private List<Floor> mAvailableFloors;
//    private List<Floor> mRequesFloors = new ArrayList<>();
//    private Floor mCurrentFloor;
//    private boolean mServiceBreak = false;
//    private Weight mMaxLoad;
//    Direction mDirection = Direction.DOWN;
