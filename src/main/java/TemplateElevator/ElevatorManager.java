package TemplateElevator;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

/**
 * Created by RENT on 2017-08-21.
 */
public class ElevatorManager {

    AtomicBoolean isWorking = new AtomicBoolean(false);

    List<Elevator> mElevators;



    BlockWithElevators  mBlockWithElevators;
    HashMap<Floor, Direction> mRequest;

    public ElevatorManager(BlockWithElevators mBlockWithElevators) {
        this.mBlockWithElevators = mBlockWithElevators;
    }


    public AtomicBoolean getIsWorking() {
        return isWorking;
    }

    public void setIsWorking(AtomicBoolean isWorking) {
        this.isWorking = isWorking;
    }

    public List<Elevator> getmElevators() {
        return mElevators;
    }

    public void setmElevators(List<Elevator> mElevators) {
        this.mElevators = mElevators;
    }

    public HashMap<Floor, Direction> getmRequest() {
        return mRequest;
    }

    public void setmRequest(HashMap<Floor, Direction> mRequest) {
        this.mRequest = mRequest;
    }

//    public void nextStep(){
//        //po tej metodzie winda ma wykonac nastepny ruch
//    }

    public boolean startWork(){

        if(isWorking.get()){
            return false;
        }
        isWorking.set(true);

        new Thread(() -> {while(isWorking.get()){
            //implementacja moveElevators i printState, mamy blok, ma liste wind, ruszamy widnami, zakladamy,
            // ze kazda widna ma metode nextStep
            // po metodzie nextStep winda wykona nastepny tuch, w move elevators i printState mamy uzyc streamow
            moveElevators();

            printState();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }}).start();

        return true;
    }
    public void printState(){
        //streamami
        mElevators.stream().forEach(new Consumer<Elevator>() {
            @Override
            public void accept(Elevator elevator) {
              elevator.nextStep();
            }
        });
        mElevators.stream().forEach(elevator -> System.out.println(elevator.toString()));
        //dodalem V
        mElevators.stream().forEach(elevator -> System.out.println(elevator.getmMaxLoad()));
        mElevators.stream().forEach(elevator -> System.out.println(elevator.getmCurrentFloor()));

        mElevators.stream().forEach(elevator -> System.out.println(elevator.getAvailableFloors()));
        mElevators.stream().forEach(elevator -> System.out.println(elevator.getmDirection()));

    }

    public void moveElevators(){

    }

}
