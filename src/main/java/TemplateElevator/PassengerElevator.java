package TemplateElevator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-08-21.
 */
public class PassengerElevator extends Elevator{



    List<ElevatorUser> mPersonOnBoard = new ArrayList<>();
    private short mMaxLoadPerson;
    QueueChecker mQueueChecker;
    Direction mDirection;

    public PassengerElevator(QueueChecker mQueueChecker) {
        this.mQueueChecker = mQueueChecker;
    }

    public QueueChecker getmQueueChecker() {
        return mQueueChecker;
    }

    public void setmQueueChecker(QueueChecker mQueueChecker) {
        this.mQueueChecker = mQueueChecker;
    }

    public PassengerElevator() {
        this.mPersonOnBoard = mPersonOnBoard;
    }

    @Override
    public void setmMaxLoad(Weight mMaxLoad) {
        super.setmMaxLoad(mMaxLoad);

        float safeWeight = mMaxLoad.mAmount * 0.9f;

        float typicalPersonWeight = ElevatorUser.AVAREGE_WEIGHT_OF_PERSON_KG;

        if(mMaxLoad.getMetrics == WeightMetrics.LB){
            typicalPersonWeight *= 2.2;
        }

        mMaxLoadPerson = (short) (safeWeight/typicalPersonWeight);
        //cos tu dokonczyc
    }
    @Override
    public void nextStep() {

        if (mPersonOnBoard.size() == 0){
            mDirection = Direction.NONE;
        }

        // krok pierwszy
        for (ElevatorUser aPersonOnBoard : mPersonOnBoard){
            if ( mCurrentFloor.equals(aPersonOnBoard.getmWantToGoFloor())){
                mPersonOnBoard.remove(aPersonOnBoard);
                System.out.println(" Osoba wyszla z windy na pietrze numer: " + mCurrentFloor);
            }
            return;
        }

        // krok drugi
        boolean anybodyGoToElevator = false;
        if(mQueueChecker.isAnyQueueOnFloor(getmCurrentFloor())){
            List<ElevatorUser> queue = mQueueChecker.getQueue(getmCurrentFloor());

            int size = queue.size();
            ElevatorUser elevatorUser;
            for (int i = 0; i < size; i++){
                elevatorUser = queue.get(i);
                if(mDirection == Direction.NONE){
                    mDirection = elevatorUser.getDirection();
                }
                //poprawic
                if (elevatorUser.getDirection() == mDirection){
                    mPersonOnBoard.add(elevatorUser);
                    mQueueChecker.removePersonFromQueque(elevatorUser);
                    anybodyGoToElevator = true;
                }
            }
        }
        if (anybodyGoToElevator){
            return;
        }
        //krok trzeci

        if (mDirection == Direction.UP){
            getmCurrentFloor().upFloor();
        }else {
            getmCurrentFloor().downFloor();
        }
    }
        /*
        sprawdzamy czy jest jakis user, ktory chce wyjsc, jak juz to zrobimy to
        pytamy czy mamy kogos wziac z tego pietra,
        1. sprawdzamy czy na tym pietrze, na ktorym jestesmy mozemy kogos wysadzic + souty informujace o wszystkim
        sprawdzamy current floor, czy reguest floor porownujemy te dwie, jak tak usuwamy usera,
        przez np.metode deleteuser czy cos takiego (chyba zrobione)
        2. przyjmujemy nowych do windy, musimy sprwdzic cyz w kolejce sa jacys oczekujacy(bierzemy hashMape
        jestesmy na pietrze current, sprawdzamy czy jes tkolejka, jak tak to ja pobieramy, dostajemy liste osob i
         spradzamy po kolei czy ktoras z osob jedzie w kierunku tym, w ktorym my jedziemy
        3.
         */
}
//poprawic co na czerwono i powinno smigac - naprawiono to na czerwono
//     plus to trzeba zrobic v i bedzie smigac
//zadanie na weekend, wywolujemy metode start przed pobraniem uzytkownikow(to co pisalismy 24 chyba albo 23 ) podspodem pobieramy uzytkownikow
//i jezeli mamy to utworzone to tam w petli dodaje sie do kolejki, jak uruchomilismy start to winda bedzie caly czas chodzic
//jak puscimy metode start, dodajemy uzytkownikow to windo nie ruszy, zeby dzialalo musimy dopisac
//zeby sprawdzlaa czy sa jacys chetni do pisania(pobrac jeszcze raz liste wszystkich userow) zakladamy, zeby ta winda jecala najpierw  w gore
//i jade tam i wtedy tam sprawdza
