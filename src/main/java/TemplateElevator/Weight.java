package TemplateElevator;

/**
 * Created by RENT on 2017-08-21.
 */
//dopisac testy jednostkowe na zajeciach
public class Weight {

    float mAmount;
    WeightMetrics mWeightMetrics;
    public WeightMetrics getMetrics;

    public WeightMetrics getGetMetrics() {
        return getMetrics;
    }

    public void setGetMetrics(WeightMetrics getMetrics) {
        this.getMetrics = getMetrics;
    }



    public Weight(float aAmount, WeightMetrics aWeightMetrics) {
        this.mAmount = aAmount;
        this.mWeightMetrics = aWeightMetrics;

    }
    //metoda ma za zadanie zwrocic wage zapisana w obiekcie w podanej metryce
    //metryka, w ktorej chcemy pobrac dane
    public float getWeight(WeightMetrics aWeightMetrics) {
        //ma zwracac wage w podanej parametrze aWeightMetrics metryce


        if (mWeightMetrics == aWeightMetrics) {
            return mAmount;
        } else if (mWeightMetrics == WeightMetrics.KG) {
            return mAmount * 2.2f;
        } else {
            return 0.4f * mAmount;
        }
    }
    @Override
    public String toString() {

       return " || Waga: " + mAmount + " " + mWeightMetrics;
    }
}
