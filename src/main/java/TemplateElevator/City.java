package TemplateElevator;

/**
 * Created by RENT on 2017-08-22.
 */
public enum City {




    LUBLIN("Lublin"),
    WARSZAWA("Warszawa"),
    GDAŃSK("Gdańsk"),
    ŁÓDŹ("Łódź"),
    KRAKÓW("Kraków"),
    KATOWICE("Katowice"),
    WROCŁAW("Wrocław"),
    ZIELONA_GÓRA("Zielona Góra"),
    STALOWA_WOLA("Stalowa Wola"),
    RZESZÓW("Rzeszów"),
    TARNÓW("Tarnów"),
    GDYNIA("Gdynia"),
    SZCZECIN("Szczecin"),
    POZNAŃ("Poznań");

    String mCityName;

    City (String aCityName){
        mCityName = aCityName;
    }

    public String getmCityName() {
        return mCityName;
    }


}
