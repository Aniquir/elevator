package TemplateElevator;

/**
 * Created by RENT on 2017-08-21.
 */
public class ElevatorUser extends Person{

    public static final int AVAREGE_WEIGHT_OF_PERSON_KG = 90;

    boolean isInElevator;
    Floor mWantToGoFloor;
    Floor mCurrentFloor;
    private Floor currentFloor;

    public ElevatorUser(String firstName, String name, Address mAddress, Weight mWeight) {
        super(firstName, name, mAddress, mWeight);
    }


    public ElevatorUser() {
        super();
    }

    public boolean isInElevator() {
        return isInElevator;
    }

    public void setInElevator(boolean inElevator) {
        isInElevator = inElevator;
    }

    public Floor getmWantToGoFloor() {
        return mWantToGoFloor;
    }

    public void setmWantToGoFloor(Floor mWantToGoFloor) {
        this.mWantToGoFloor = mWantToGoFloor;
    }

    public Floor getmCurrentFloor() {
        return mCurrentFloor;
    }

    public void setmCurrentFloor(Floor mCurrentFloor) {
        this.mCurrentFloor = mCurrentFloor;
    }

    @Override
    public String toString() {
        return super.toString() + " || " + isInElevator + " " +
                mWantToGoFloor + " " + mCurrentFloor;
    }
    public Direction getDirection(){
        //napisac metode, ktora okresla kierunek, program oblicza, w ktorą strone jedzie osoba,
        //w ifie przedstawiam mWantToGoFllor czy jest wiekszy od CurrentFloor, jakos tak, albo odwrotnie
        //i zmieniam result na inny jezeli jest

        Direction result = Direction.DOWN;
        if ( getmWantToGoFloor().mFloorNo > getmCurrentFloor().mFloorNo){
            result = Direction.UP;
        }
        return result;
    }

    public Floor getCurrentFloor() {
        return currentFloor;
    }
}
