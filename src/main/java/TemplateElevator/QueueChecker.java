package TemplateElevator;

import java.util.List;

/**
 * Created by RENT on 2017-08-25.
 */
public interface QueueChecker {

    boolean isAnyQueueOnFloor(Floor floor);
    List<ElevatorUser> getQueue(Floor floor);

    void removePersonFromQueque(ElevatorUser elevatorUser);


}
