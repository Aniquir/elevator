package TemplateElevator;

/**
 * Created by RENT on 2017-08-21.
 */
public class Floor {

    int mFloorNo;


    public Floor(int i) {
        this.mFloorNo = i;
    }

    @Override
    public boolean equals(Object obj) {

        boolean result = false;

        if (obj instanceof Floor) {

            Floor temp = (Floor) obj;

            if(mFloorNo == temp.mFloorNo){
                result = true;
            }
        }
        return result;
    }

    @Override
    public int hashCode() {
        return mFloorNo;
    }

    public void upFloor(){
        mFloorNo++;
    }

    public void downFloor(){
        mFloorNo--;
    }

}
