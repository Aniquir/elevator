package TemplateElevator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by RENT on 2017-08-23.
 */
public class BlockWithElevators extends Block implements QueueChecker {

    private List<Elevator> mElevators;
    private HashMap<Floor, List<ElevatorUser>> mElevatorQueue;

    public BlockWithElevators(List<Floor> floors) {
        super(floors);

        mElevatorQueue = new HashMap<>();

    }


    public List<Elevator> getElevators(){
        return mElevators;
    }

    public void setElevators(List<Elevator> mElevators){
        this.mElevators = mElevators;
    }

    @Override
    public boolean hasElevator() {
        return true;
    }

//dodales
    public HashMap<Floor, List<ElevatorUser>> getmElevatorQueue() {
        return mElevatorQueue;
    }

    public void setmElevatorQueue(HashMap<Floor, List<ElevatorUser>> mElevatorQueue) {
        this.mElevatorQueue = mElevatorQueue;
    }
//koniec dodania

    public void addElevatorUserToQueQue(Floor aFloor, ElevatorUser aElevatorUser){

        List<ElevatorUser> listOfQueueOnFloor = mElevatorQueue.get(aFloor);
//tutaj tez zakomentowane drukowanie dla czystosci kodu
        if(listOfQueueOnFloor != null){
            listOfQueueOnFloor.add(aElevatorUser);
//            System.out.println("znalezlismy pietro, dodajemy uzytkownika do istniejacego pietra");
        }else{
            ArrayList<ElevatorUser> elevatorUsers = new ArrayList<>();
            elevatorUsers.add(aElevatorUser);

            mElevatorQueue.put(aFloor, elevatorUsers);
//            System.out.println("nie mamy pietra, dodajemy nowe pietro i uzytkownika do niego");
        }

       //pobrac liste uzytkownikow na danym pietrze i dodac od niej
    }

    @Override
    public boolean isAnyQueueOnFloor(Floor floor) {

        return mElevatorQueue.containsKey(floor);
        /*
        bedziezwracac boolean a parmetrem bedzie floor
        musimy teraz znalac klasy, dzieki, ktorym bedziemy w stanie tego ziamplementowac, to bedzie
        to klasa BlockWithElevators
        */

    }

    @Override
    public List<ElevatorUser> getQueue(Floor floor) {
        return null;
    }

    @Override
    public void removePersonFromQueque(ElevatorUser aElevatorUser) {
        List<ElevatorUser> elevatorUsers = mElevatorQueue.get(aElevatorUser.getCurrentFloor());
        elevatorUsers.remove(aElevatorUser);
    }

    @Override
    public List<ElevatorUser> getPersonsFromFloor(Floor floor) {

        return mElevatorQueue.get(floor);
        /*
        bedzie zwracac liste ElevatorUser, bedzie miala parametr floor

        */
    }
}
