package TemplateElevator;

import TemplateElevator.utils.PostalCodeUtils;

import java.util.*;

/**
 * Created by RENT on 2017-08-21.
 */
public class Main2 {

    public static void main(String[] args) {

//        Elevator elevator = new Elevator();
//
//        Gson gson = new Gson();
//
//        String out = gson.toJson(elevator);
//
//        try (BufferedWriter writer = new BufferedWriter(new FileWriter("elevator.txt"))){
//
//            writer.write(gson.toJson(elevator));
//        } catch (IOException e){
//
//        }
        //--- 22.08.2017
        //tworze persona, adres, i wage

        Address adres = new Address(City.GDAŃSK, "Właścicielska", 30, 20, "20-334");
        Weight weight = new Weight(70, WeightMetrics.KG);

        Person oneOfUs = new Person("Gabriel", "Gregorowicz", adres, weight);

//        System.out.println(oneOfUs.toString());

        Address adres2 = new Address(City.POZNAŃ, "Zwycieska", 40, 10, "21-084");
        Weight weight2 = new Weight(160, WeightMetrics.LB);
        ElevatorUser onOfUsInElevator = new ElevatorUser("January", "Bonuary", adres2, weight2);
//        System.out.println(onOfUsInElevator.toString());


        if (PostalCodeUtils.isCorrect("20-919", "POL")){
//            System.out.println("ok");
        }else {
//            System.out.println("nie ok");
        }
        // 23.08.2017

//zakomentowalem drukowana, zeby nie zasmiecaly(28.08.2017)
        List<Floor> mFloors = new ArrayList<>();
        //dodaje winde
        Elevator elevator = new PassengerElevator();

        BlockWithElevators blockWithElevators = new BlockWithElevators(mFloors);

        blockWithElevators.setmAddress(adres);
        blockWithElevators.setmFloors(mFloors);
        blockWithElevators.setElevators(Arrays.asList(elevator));

        //dodaje pietra do bloku
        for (int i = -2; i <= 10; i++){
            mFloors.add(new Floor(i));
        }

        elevator.setmAvailableFloors(blockWithElevators.getmFloors());
        elevator.setmCurrentFloor(new Floor(0));
        elevator.setmMaxLoad(new Weight(800, WeightMetrics.KG));

        ElevatorUser elevatorUser = new ElevatorUser();
        ElevatorUser elevatorUser2 = new ElevatorUser();

        blockWithElevators.addElevatorUserToQueQue(new Floor(1), elevatorUser);
        blockWithElevators.addElevatorUserToQueQue(new Floor(1), elevatorUser2);

        Floor floor = new Floor(2);
        blockWithElevators.addElevatorUserToQueQue(floor, elevatorUser);
        blockWithElevators.addElevatorUserToQueQue(floor, elevatorUser2);
//        System.out.println();
//        System.out.println();
//--- 24.08.2017

        Scanner sc = new Scanner(System.in);
        // 27 08 2017 (to do)
        // wdodac wyjatki( przynajmniej jeden ) - jako tak zrobione
        //ten random ma byc po to, zeby wylosowac pietro, na ktorym wsiada i wysiada osoba
        //i jeszcze trzeba dodac, ze nowa osoba wsiada co 15s - zrobic w domu, juz prawie wszystko dziala, ale jeszcze troszke brakuje
        Random random = new Random();

//dopisuje try catch, zeby wprowadzic obsluge wyjatkow - jako tako dziala, ale przedebagowac czy dobrze
        for (int i = 0; i < i + 1; i++) {

            System.out.println("Podaj numer piętra, na ktorej znajduje sie osoba: ");
            String skadJedziemy = sc.nextLine();
            int x = Integer.parseInt(skadJedziemy);
            Floor mCurrentFloor = new Floor(x);

            ElevatorUser eUser = new ElevatorUser();
            System.out.println("Podaj imie osoby: ");

            String imieOsoby = sc.nextLine();
            eUser.setFirstName(imieOsoby);
            eUser.setmCurrentFloor(mCurrentFloor);

            System.out.println("A teraz podaj numer piętra, na ktore dana osoba chce sie przemiescic");

            String dokadJedziemy = sc.nextLine();
            int x1 = Integer.parseInt(dokadJedziemy);
            Floor mWantToGoFloor = new Floor(x1);

            eUser.setmWantToGoFloor(mWantToGoFloor);


            try {
                System.out.println("chcesz dodac kolejna osobe? (Y/N)");

                String dodawanieNastepnejOsoby = sc.nextLine();

                if (dodawanieNastepnejOsoby.toLowerCase().compareTo("n") == 0 ) {
                    System.out.println("kończę program");
                    break;

                } else if (dodawanieNastepnejOsoby.toLowerCase().compareTo("y") == 0){

                    continue;

                }else {
                    //stworzony zostal nowy wyjatek, poniewaz w bazie wyjatkow nie bylo go, to mozna sobie bylo napisac swoj(super to jest)
                    //potrzebna jeszcze implementacja, by sprawdzal czy to jest string
                    throw new MyFirstException();
                }
            } catch (MyFirstException e) {
                System.out.println("nie podano litery, może teraz się uda: ");

                String dodawanieNastepnejOsoby = sc.nextLine();

                if (dodawanieNastepnejOsoby.equals("N") || dodawanieNastepnejOsoby.equals("n")) {
                    System.out.println("kończę program");
                    break;

                }
            }
        }
    }
}
/*

22.08.2017
piszemy sterownik do windy:
1.zdefiniuj jakie dane sa potrzebne
2.zdefiniuj jakie przypadki moga sie zdarzyc(user story)
3.zdefiniuj forme danych wejsciowych
4.ile czasu zajmie zaimplementowanie tego algorytmu
5.zaimplementuj go
6.dodaj mozliwosc recznego dodawania osob cetnych na przejazdzake winda
7.chetni: przestaw graficznie dzialanie windy(ze swingiem, nie bedziemy tego robic)
8.chetni: zoptymalizuj dzialanie windy

23.08.2017
+inne
naprawic nullPointerException BlockWithElevators:52 i Main2:70 - naprawione

24.08.2017
od Tomelko co mamy zrobic:
pętla nieskończona w której dodajemy pewsona do windy
       wpisując dwie liczby
       piętro na którym jest
       i na którym chce się znaleźć

       wyjście z pętli po podaniu słowa Exit
       zrobione ^
       -- -- --
       to do v
       dodac nowy wątek - uruchamiamy winde, mamy tak reguesty, ktore dzialaja turowo,
       dodajemy ludzi jednoczesnie i winde dziala jednoczesnie i menager tym steruje

       metda start sprawdza czy: juz wystartowalismy, jak nie to pytamy czy chcemy wystarrotwac i jak tak to
       startujemy, po wystartowaniu w pętli (jezeli jestes wystartowana) to cos tam zrob, jezeli chcemy
       wystartowac winde to...

       tamta petle w watek czy cos tma ma wspolnego z watkiem i wywoluje sie co sekunde Thread..sleep(1000)
       co sekunde mowimy jej: wykonaj nastepny krok czy cos takiego

       co next step bedzie sie ruszac do dolu lub do gory o jedno pietro

25.08.2017
        tamto z wczoraj jeszcze nie zrobione

28.08.2017
        zwrocic szczegolna uwage na streamy i strukture programu od Tomka

 */
