package TemplateElevator;

import java.util.List;

/**
 * Created by RENT on 2017-08-23.
 */
public abstract class Block {

    List<Floor> mFloors;
    Address mAddress;

    public Block(List<Floor> floors) {
        mFloors = floors;
    }

    public Block() {

    }

    public abstract boolean hasElevator();

    public List<Floor> getmFloors() {
        return mFloors;
    }

    public void setmFloors(List<Floor> mFloors) {
        this.mFloors = mFloors;
    }

    public Address getmAddress() {
        return mAddress;
    }

    public void setmAddress(Address mAddress) {
        this.mAddress = mAddress;
    }

    public abstract List<ElevatorUser> getPersonsFromFloor(Floor floor);
}
