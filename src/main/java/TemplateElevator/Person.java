package TemplateElevator;

/**
 * Created by RENT on 2017-08-22.
 */
public class Person {

    String firstName;
    String name;

    Address mAddress;
    Weight mWeight;

    public Person(String firstName, String name, Address mAddress, Weight mWeight) {
        this.firstName = firstName;
        this.name = name;
        this.mAddress = mAddress;
        this.mWeight = mWeight;
    }

    public Person() {

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getmAddress() {
        return mAddress;
    }

    public void setmAddress(Address mAddress) {
        this.mAddress = mAddress;
    }

    public Weight getmWeight() {
        return mWeight;
    }

    public void setmWeight(Weight mWeight) {
        this.mWeight = mWeight;
    }

    @Override
    public String toString() {
        return "|| Dane osobowe: " + firstName + " " + name + " " + mAddress + " " + mWeight;
    }


}
